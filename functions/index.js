const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.sendPushNotificationDebate = functions.firestore
  .document('activities/WeeklyActivities/DebateClub/{eventID}')
  .onCreate((snap, context) => {
    var topic = "events"
    console.log(snap.data());
    const item = snap.data();
    let message = {
      notification: {
        title: item.name,
        body: item.shortDescription,
        // sound: "default"
     },
     topic: topic
    }
    return admin.messaging().send(message);
  })

exports.sendPushNotificationBusiness = functions.firestore
  .document('activities/WeeklyActivities/BusinessClub/{eventID}')
  .onCreate((snap, context) => {
    var topic = "events"
    console.log(snap.data());
    const item = snap.data();
    let message = {
      notification: {
        title: item.name,
        body: item.shortDescription,
        // sound: "default"
     },
     topic: topic
    }
    return admin.messaging().send(message);
  })

exports.sendPushNotificationEngineering = functions.firestore
  .document('activities/WeeklyActivities/EngineeringClub/{eventID}')
  .onCreate((snap, context) => {
    var topic = "events"
    console.log(snap.data());
    const item = snap.data();
    let message = {
      notification: {
        title: item.name,
        body: item.shortDescription,
        // sound: "default"
     },
     topic: topic
    }
    return admin.messaging().send(message);
  })

exports.sendPushNotificationCharacter = functions.firestore
  .document('activities/WeeklyActivities/CharacterClub/{eventID}')
  .onCreate((snap, context) => {
    var topic = "events"
    console.log(snap.data());
    const item = snap.data();
    let message = {
      notification: {
        title: item.name,
        body: item.shortDescription,
        // sound: "default"
     },
     topic: topic
    }
    return admin.messaging().send(message);
  })
  
  

// exports.sendPushNotification = functions.firestore
//   .document("activities/{WeeklyActivities}")
//   .onUpdate((change, context) => {
//     // gets standard JavaScript object from the new write
//     // const writeData = event.data.data();
//     console.log(change.after.data());
//     // access data necessary for push notification 
//     // const sender = writeData.uid;
//     // const senderName = writeData.name;
//     // const recipient = writeData.recipient;
//     // the payload is what will be delivered to the device(s)
//     let payload = {
//       notification: {
//         title: "Test",
//         body: "Testing cloud functions",
//         sound: "default"
//      }
//     }
//     // either store the recepient tokens in the document write
//     // const tokens = writeData.tokens;  
    
//     // return admin.messaging().sendAll(payload.notification);
    
//     const deviceToken = "cm-Vh_OAVmQ:APA91bExWPAIArloZHmOzCv7UA03BXpkxsTj7JNJonfhnW0LURG8QfczUNFYtWAXa-LNowEn9nZN0H8hbGJ2MzNrU_1qmJcxOQoViIkWCfCWAVtAYHJjO4qcfT-TYCtxv8_j9TJUg5tx";
//     return admin.messaging().sendToDevice(deviceToken, payload);
//     // return functions
//     //   .firestore
//     //   .collection("user_data_collection/recipient")
//     //   .get()
//     //   .then(doc => {
//     //     //  pushToken = doc.data().token;
//     //      // sendToDevice can also accept an array of push tokens
//     //      return admin.messaging().sendToDevice(pushToken, payload);
//     //   });
// });

// exports.sendBusinessNotifications = functions.firestore
//     .document()
