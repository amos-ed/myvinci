# MyVinci

Aceasta este prima versiune a aplicatiei MyVinci. Va trebui sa o descarcati de aici folosind comanda
`git clone`. Dupa ce ati descarcat aplicatia, puteti sa va uitati prin fisierele cu numele de `readme.txt` 
pentru a vedea cateva lucruri despre cum ne vom structura codul.

Mai departe, va sugerez sa aruncati un ochi peste [React Native](https://facebook.github.io/react-native)
pentru a vedea mai exact cum functioneaza acest limbaj. Aveti exemple inclusiv in fisierul AppScreens.

## !!! Super important !!!

Dupa ce ati descarcat acest repository sau ati dat un update la proiect, nu uitati sa rulati comanda `npm install`

# Setup

Pentru a putea incepe sa lucrati propriu-zis cu React Native, veti avea nevoie de cateva tool-uri principale:

- [Git](https://git-scm.com/downloads) pentru a putea sa lucram mai eficient in echipa, avand in vedere ca acest proiect are proportii mult mai mari fata de un simplu site.
- [Node.js si npm](https://nodejs.org/en/download/) pentru a putea porni aplicatia. 
- Python 2 si JDK 8. Acestea pot sa fie instalate prin Chocolatey. Pornesti un Command Prompt ca administrator si rulezi urmatoarele comenzi: 
    - `@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"`
    - `choco install -y nodejs.install python2 jdk8`
- [Android Studio](https://developer.android.com/studio) tot pentru a porni aplicatia (vedeti mai jos cum se configureaza). Cand il instalati, alegeti "Custom installation" si aveti grija sa bifati urmatoarele lucruri: 
    - Android SDK
    - Android SDK Platform
    - Performance (Intel ® HAXM) (Pentru procesor AMD intrati [aici](https://android-developers.googleblog.com/2018/07/android-emulator-amd-processor-hyper-v.html))
    - Android Virtual Device 
- React Native CLI: Aceasta se instaleaza ruland comanda: `npm install -g react-native-cli`

# Configurare Android Studio si React Native 

Pentru a putea sa incepem sa lucram, mai trebuie facute inca cateva lucruri. V-as sugera sa luati pas cu pas urmatoarele lucruri ca sa evitam unele posibile probleme.

1. Deschide Android Studio
2. Open existing A.S. Project -> (cautati MyVinci si deschideti fisierul `android`). Dupa asta, Android Studio va descarca toate librariile de care este nevoie (nu va atingeti de fisiere pana cand nu termina).
3. Apasati tasta `Windows` si scrieti "Edit the system environment variables" si apasati enter cand va apare
4. In coltul din dreapta jos este un buton pe care scrie "Environment Variables...". Dati click pe el
5. Apasati pe butonul "New..." din partea de jos. Se va deschide o fereastra in care va trebui sa introduceti urmatoarele lucruri: ![alt text](https://facebook.github.io/react-native/docs/assets/GettingStartedAndroidEnvironmentVariableANDROID_HOME.png) 

    In loc de "hramos" puneti username-ul vostru. 
    
    Daca nu ati instalat SDK-ul in locatia lui default, intrati in Android Studio -> File -> Settings -> Appearance & Behavior -> System Settings -> Android SDK.
6. Restartati toate Command Prompt-urile pe care le aveti deschise ca sa fim siguri ca variabila a ramas setata

Dupa ce e gata partea asta, avem nevoie de un telefon pe care sa rulam aplicatia. Aici sunt 2 variante: fie lucrati pe un Android Emulator, fie pe un telefon fizic.

# Configurare Android Emulator

1. Intrati in Android Studio. In coltul din dreapta sus va uitati dupa AVD Manager. Are o iconita care arata ceva de genul asta ![alt text](https://facebook.github.io/react-native/docs/assets/GettingStartedAndroidStudioAVD.png)
2. In fereastra deschisa selectati "Create new virtual device".
3. Selectati ce fel de telefon preferati de acolo (eu de obicei aleg Nexus 5X). Next
4. La partea urmatoare intrati la x86 Images si selectati una dintre variantele "Pie" (nu conteaza foarte mult care dintre ele).
5. Next, next pana e gata.
6. In AVD Manager selectati emulatorul pe care l-ati creat si porniti-l.


# Configurare telefon

Ca sa folosim un telefon putem sa lucram fie prin conexiune USB, fie prin Wi-Fi. 

Pentru conexiunea USB am gasit un tutorial pe [StackOverflow](https://stackoverflow.com/questions/36609523/how-to-run-react-native-app-on-android-phone) extrem de simplu (mergeti la al treilea raspuns, cel cu multe imagini).

Pentru conexiunea prin Wi-Fi este necesar ca mai intai sa configuram conexiunea prin cablu pentru a instala aplicatia (vezi mai jos cum se porneste aplicatia). Dupa ce e pornita mai avem de facut urmatoarele lucruri:

1. Pe calculator deschide un Command Prompt si scrie `ipconfig`. Acolo uita-te dupa o linie unde scrie "IPv4 Address".
2. Scutura putin telefonul (nu glumesc). O sa iti apara un meniu in centrul ecranului.
3. Apasa pe "Dev Settings"
4. Intra la "Debug server host & port for device"
5. Copiada adresa IPv4 de pe calculator si la sfarsitul ei adauga ":8081" (portul de pe care se va incarca aplicatia).
6. Acum nu mai trebuie sa tii cablul conectat tot timpul. Singurul lucru de care trebuie sa te asiguri e ca telefonul si calculatorul sunt conectate la aceeasi retea.


# Pornirea aplicatiei

Dupa ce ti-ai setat emulatorul sau telefonul, urmeaza sa pornim propriu-zis aplicatia. Nu uitati sa va asigurati ca telefonul sau emulatorul sunt pornite si gata de treaba.

1. Deschideti un Command Prompt si navigati in fisierul MyVinci.
2. Rulati comanda `react-native run-android`
3. Asteptati cateva minute (prima data dureaza mai mult).
4. There you go! 

## Pentru orice fel de intrebari, scrieti pe grup. 

# Spor la lucru! 