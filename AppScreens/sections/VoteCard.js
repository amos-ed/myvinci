import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards'

export default class VoteCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    renderImportance = (importance) => {
        //Deoarece importanta este stocata ca si numar in firebase, avem nevoie de o functie
        //care sa ne returneze ca string importanta unui anumit vot
        if (importance === 1) return "Scazuta";
        if (importance === 2) return "Medie";
        if (importance === 3) return "Ridicata";
        if (importance === 4) return "Urgenta";
    }


    getImportanceStyle = (importance) => {
        //In functie de importanta anuntului, avem nevoie de alte atribute de css
        //Astfel, pentru a usura treaba, returnam direct obiectul din StyleSheet de care
        //avem nevoie, in functie de importanta anuntului
        if (importance === 1) return styles.small;
        if (importance === 2) return styles.medium;
        if (importance === 3) return styles.high;
        if (importance === 4) return styles.urgent;
    }


    render() {
        const item = this.props.item;
        return (
            <Card style = {styles.container}>
                <CardImage 
                    source = {require('../activities/img/vote.png')}
                    style = {{
                        width: '100%',
                        height: '60%',
                        backgroundColor: 'rgb(179, 1, 1)'
                    }}
                />
                <CardTitle title = {item.question} subtitle = {`Votul este disponibil pana in ${item.endDate}`} />
                {/* <CardTitle subtitle = {`Importanta: ${this.renderImportance(item.importance)}`} style = {this.getImportanceStyle(item.importance)}/> */}
                <Text style = {this.getImportanceStyle(item.importance)}>Importanta: {this.renderImportance(item.importance)}</Text>
                <CardContent text = {item.infoslide} />
                <CardAction separator = {true} inColumn = {false}>
                    <CardButton 
                        title = "Voteaza!"
                        onPress = {() => this.props.navigate()}
                        color = "black"
                    />
                </CardAction>
            </Card>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'rgb(240, 240, 240)',
    },
    small: {
        color: 'green',
        marginLeft: '4%'
    },
    medium: {
        color: '#e0c01d',
        marginLeft: '4%'
    },
    high: {
        color: '#f5580a',
        marginLeft: '4%'
    },
    urgent: {
        color: 'red',
        marginLeft: '4%'
    }
})