import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import firebase from 'react-native-firebase';
import { FlatList } from 'react-native-gesture-handler';

export default class VoteAnswers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.vote,
            answers: [],
            loading: true,
            pentru: 0,
            impotriva: 0,
            abtinere: 0,
        }
    }

    componentDidMount() {
        this.getVoteAnswers();
    }

    getVoteAnswers = () => {
        const votes = firebase.firestore().collection('votes').doc(this.state.id);
        votes.get()
            .then(doc => {
                const item = doc.data();
                var pentru = 0, impotriva = 0, abtinere = 0;
                item.answers.map(i => {
                    if (i.answer === "Pentru") pentru ++;
                    if (i.answer === "Impotriva") impotriva ++;
                    if (i.answer === "Abtinere") abtinere ++;
                })
                this.setState({ answers: item.answers, loading: false, impotriva: impotriva, pentru: pentru, abtinere: abtinere });
            })
    }

    render() {
        if (this.state.loading === true) {
            return <View></View>
        }
        else return (
            <View style = {styles.container}>
                <Text style = {styles.stat}>Pentru: {this.state.pentru}</Text>
                <Text style = {styles.stat}>Impotriva: {this.state.impotriva}</Text>
                <Text style = {styles.stat}>Abtineri: {this.state.abtinere}</Text>
                <FlatList
                    data = {this.state.answers}
                    renderItem = {({ item }) => (
                        <View style = {styles.displayInline}>
                            <Text style = {styles.name}>{item.name}: </Text>
                            <Text style = {styles.answer}>{item.answer}</Text>
                        </View>
                    )}
                    keyExtractor = {item => item.email}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    displayInline: {
        flexDirection: 'row',
        margin: '5%'
    },
    name: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    answer: {
        fontSize: 16
    },
    stat: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
    }
})