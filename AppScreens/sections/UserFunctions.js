import firebase from "react-native-firebase";
import React from 'react';

const users = firebase.firestore().collection('users');
export class UserFunctions extends React.Component {
    constructor(props) {
        super(props);
    }

    static async SearchByEmail(userEmail) {
        var user = '';
        await users.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const item = doc.data();
                    if (item.email === userEmail) {
                        user = item.firstName.toString() + " " + item.lastName.toString();
                    }
                })
            })
            .catch(error => console.log(error));
        return user.toString();
    }

    static async GetUserDetails(userEmail) {
        var user = {};
        await users.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const item = doc.data();
                    if (item.email === userEmail) {
                        user = {
                            email: userEmail,
                            firstName: item.firstName,
                            lastName: item.lastName,
                            level: item.level,
                            userType: item.userType,
                            xp: item.xp,
                        }
                    } 
                })
            })
            .catch(error => console.log(error));
        return user;
    }

    static async getUserByName (name) {
        var user = {};
        await users.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const item = doc.data();
                    if ((item.firstName + item.lastName).localeCompare(name) === 0) {
                        user = {
                            name: name,
                            email: item.email,
                            level: item.level,
                            userType: item.userType,
                            xp: item.xp,
                        }
                    }
                })
            })
        return user;
    }
}