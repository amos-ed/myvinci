    Acesta este folderul de sectiuni.
    O sectiune este o parte a unei pagini. In acest folder vom pune componente care vor putea mai 
tarziu sa fie importate in cadrul activitatilor si sa apara ca o parte a acesteia. 
    De exemplu, daca vom vrea sa avem un meniu de navigare care sa apara in toate paginile, nu prea are
rost sa il scriem de fiecare data cand facem o pagina noua. Ii facem un component aici in "sections", il 
importam si il folosim de fiecare data cand avem nevoie de el.
Spor!