import React from 'react';
import firebase from 'react-native-firebase';


export default class EventFunctions extends React.Component {
    constructor(props) {
        super(props);
    }

    static getClubCollectionName (club) {
        if (club === "Debate") return "DebateClub";
        if (club === "Business") return "BusinessClub";
        if (club === "Character") return "Character Club";
        if (club === "Digital") return "Digital Club";
    }
    
    static async GetEvent (eventName, club) {
        const clubName = this.getClubCollectionName(club);
        const events = firebase.firestore().collection('activities').doc('WeeklyActivities').collection(clubName);
        var event = {};
        await events.get()
            .then(response => {
                response.docs.forEach(doc => {
                    if (eventName.localeCompare(doc.data().name) === 0) {
                        event = doc.data()
                    }
                })
            })
            .catch(error => {
                console.log(error);
                event = null;
            });
        return event;
    }

    static async GetEventByID (id, club) {
        const clubName = this.getClubCollectionName(club);
        const events = firebase.firestore().collection('activities').doc('WeeklyActivities').collection(clubName).doc(id);
        var event = {};
        await events.get()
            .then(doc => {
                event = doc.data();
            })   
            .catch(error => console.log(error));
        return event;
    }

    static async GetBigEvent (eventName) {
        const event = firebase.firestore().collection('activities');
        var retEvent = {};
        await event.get()
            .then(response => {
                response.docs.forEach(doc => {
                    if (doc.id !== 'WeeklyActivities' && eventName.localeCompare(doc.data().name) === 0) {
                        retEvent = doc.data();
                    }
                })
            })
            .catch(error => {
                console.log(error);
                retEvent = null;
            });
        return retEvent;
    }
}