import React, {Component} from 'react';
import {View, Text, StyleSheet, Picker, Alert} from 'react-native';
import firebase from 'react-native-firebase';
// import Dropdown from 'react-dropdown';

const options = ["Alumni", "Membru asociat", "Partener", "JMRU", "JMRP", "JMRF", "JMRD", "MRU", "MRF", "MRD", "MRP", "SG", "BD/CD"];
const userRef = firebase.firestore().collection('users');

export default class PromoteUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ddIdx: this._defaultOption(this.props.user),
        }
    }

    _defaultOption = (user) => {
        return options.find((element, index)=> {
            if (element === user.userType) return element;
        })
    }

    updateUserType = (option) => {
        const user = this.props.user;
        userRef.doc(this.props.user.id).set({
            userType: option,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            level: user.level,
            xp: user.xp,
        })
    } 

    _onSelect = (option) => {
        this.setState({ ddIdx: option });
        this.updateUserType(option);
    }

    render() {
        const user = this.props.user;
        const username = user.firstName + " " + user.lastName;
        return (
            <View>
                <Text style = {styles.name}>{username}</Text>
                <Picker 
                    selectedValue = {this.state.ddIdx}
                    onValueChange = {(itemValue, itemIndex) => this._onSelect(itemValue)}
                    enabled = {true}
                    mode = "dropdown"
                >
                    <Picker.Item label = "Alumni" value = "Alumni" />
                    <Picker.Item label = "Membru asociat" value = "Membru asociat" />
                    <Picker.Item label = "Partener" value = "Partener"/>
                    <Picker.Item label = "JMRP" value = "JMRP" />
                    <Picker.Item label = "JMRU" value = "JMRU" />
                    <Picker.Item label = "JMRF" value = "JMRF" />
                    <Picker.Item label = "JMRD" value = "JMRD" />
                    <Picker.Item label = "MRP" value = "MRP" />
                    <Picker.Item label = "MRU" value = "MRU" />
                    <Picker.Item label = "MRF" value = "MRF" />
                    <Picker.Item label = "MRD" value = "MRD" />
                    <Picker.Item label = "SG" value = "SG" />
                    <Picker.Item label = "BD/CD" value = "BD/CD" />
                </Picker>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        margin: 'auto',
    },
    name: {
        fontSize: 24,
        fontWeight: 'bold',
    }
})