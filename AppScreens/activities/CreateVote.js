import React from 'react';
import { Text, View, TextInput, StyleSheet, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Button } from 'react-native-elements';
import {UserFunctions} from '../sections/UserFunctions.js';
import { CheckBox } from 'react-native-elements';

export class CreateVote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            voteText: '',
            isPickerVisible: false,
            endDate: '',
            infoslide: '',
            importance: 1,
            importance1: true,
            importance2: false,
            importance3: false,
            importance4: false,
            user: {},
            loading: true,
        }
    }

    async componentDidMount() {
        const email = firebase.auth().currentUser.email;
        await UserFunctions.GetUserDetails(email)
            .then(response => {
                this.setState({ user: response, loading: false });
            })
            .catch(error => {
                console.log(error);
            })
        // console.log(this.renderUrgentImportance());
    }


    changePickerVisibility = () => {
        this.setState({ isPickerVisible: !this.state.isPickerVisible });
    }

    dateToString = (date) => {
        return date.toISOString().split('T')[0];
    }

    handleDatePicked = (date) => {
        this.setState({ endDate: this.dateToString(date) });
        this.changePickerVisibility();
    }

    checkIfDataExists = () => {
        if (this.state.voteText === '') {
            Alert.alert("Introdu ceea ce se va vota");
            return false;
        }
        if (this.state.endDate === '') {
            Alert.alert("Selecteaza deadline-ul pentru vot");
            return false;
        }
        return true;
    }

    addVote = async () => {
        if (this.checkIfDataExists() === false) return;
        const votes = firebase.firestore().collection('votes');
        var user = '';
        await UserFunctions.SearchByEmail(firebase.auth().currentUser.email)
            .then(response => {
                user = response;
            })
            .catch(error => console.error(error))
        const finalObj = {
            email: firebase.auth().currentUser.email,
            name: user,
            question: this.state.voteText,
            endDate: this.state.endDate,
            type: this.state.importance,
        }

        votes.add(finalObj)
            .then(() => {
                Alert.alert("Votul a fost adaugat cu succes");
                this.props.navigation.navigate('HomepageRT');
            })
            .catch(error => console.log(error));
    }

    getDateIfExists = () => {
        if (this.state.endDate !== '') {
            return `Se va putea vota pana in data ${this.state.endDate}`;
        }
        else return '';
    }

    renderHighImportance = () => {
        const type = this.state.user.userType;
        if (type === "JMRU" || type === "JMRF" || type === "JMRD" || type === "JMRP"
            || type === "MRU" || type === "MRF" || type === "MRD" || type === "MRP" || type === "BD/CD") {
            return (
                <View>
                    <CheckBox
                        title="Ridicata"
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.importance3}
                        onPress={() => this.setState({
                            importance1: false,
                            importance2: false,
                            importance3: true,
                            importance4: false,
                            importance: 3,
                        })}
                    />
                </View>
            )
        }
        else return null;
    }

    renderUrgentImportance = () => {
        const type = this.state.user.userType;
        if (type === "BD/CD") {
            return (
                <CheckBox
                    title="Urgenta"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.importance4}
                    onPress={() => this.setState({
                        importance1: false,
                        importance2: false,
                        importance3: false,
                        importance4: true,
                        importance: 4,
                    })}
                />
            )
        }
        else return null;
    }

    loadingScreen = () => {
        return <View></View>;
    }


    render() {
        if (this.state.loading === true) return this.loadingScreen();
        else return (
            <View style={styles.container}>
                <TextInput
                    placeholder="Ce se va vota..."
                    multiline={true}
                    onChangeText={(text) => this.setState({ voteText: text })}
                    value={this.state.voteText}
                />
                <TextInput
                    placeholder="Adauga un infoslide (optional)"
                    multiline={true}
                    onChangeText={(text) => this.setState({ infoslide: text })}
                    value={this.state.infoslide}
                />
                <Text>{this.getDateIfExists()}</Text>
                <Button title="Pana cand se poate vota?" onPress={() => this.changePickerVisibility()} />
                <DateTimePicker
                    mode="date"
                    isVisible={this.state.isPickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.changePickerVisibility}
                />

                <Text>Alege importanta votului</Text>
                <CheckBox
                    title="Scazuta"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.importance1}
                    onPress={() => this.setState({
                        importance1: true,
                        importance2: false,
                        importance3: false,
                        importance4: false,
                        importance: 1,
                    })}
                />


                <CheckBox
                    title="Medie"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.importance2}
                    onPress={() => this.setState({
                        importance1: false,
                        importance2: true,
                        importance3: false,
                        importance4: false,
                        importance: 2,
                    })}
                />

                {this.renderHighImportance()}

                {this.renderUrgentImportance()}

                <Button title="Submit" onPress={this.addVote} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})