import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import ActionButton from 'react-native-action-button';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VoteCard from '../sections/VoteCard';
import {UserFunctions} from '../sections/UserFunctions.js';

const votes = firebase.firestore().collection("votes");

export class VotesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            actualVotes: [],
            canVote: false,
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <Button
                    icon={
                        <Icon
                            name="add"
                            size={25}
                            color="black"
                        />
                    }
                    buttonStyle={{ backgroundColor: 'rgb(179, 1, 1)' }}
                    onPress = {navigation.getParam('createVote')}
                />
            ),
            headerStyle: {
                backgroundColor: 'rgb(179, 1, 1)',
            },
        }
    }

    navigateToCreateVote = () => {
        this.props.navigation.navigate('CreateVoteRT');
    }

    canAddVotes = async () => {
        var user = {};
        const email = firebase.auth().currentUser.email;
        await UserFunctions.GetUserDetails(email)
            .then(response => {
                user = response;
            })
            .catch(error => console.log(error));
        if (user.userType !== "Alumni" && user.userType !== "Membru asociat" && user.userType !== "Partener") {
            this.props.navigation.setParams({ createVote: this.navigateToCreateVote });
        }
        else {
            this.props.navigation.setParams({ createVote: null });
        }
    }

    componentDidMount() {
        this.checkForVotes();
        this.canAddVotes();
    }

    checkForVotes = async () => {
        //Caut in firebase toate documentele de vot in care se poate vota in data actuala.
        votes.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const date = new Date();
                    //Datele fiind formatate in modul ISO, printr-o comparare putem sa ne dam seama daca
                    //in data actuala se mai poate vota
                    //Astfel, daca data de astazi este mai mica sau egala decat data din firebase, inseamna
                    //ca inca mai putem vota
                    if (date.toISOString().split('T')[0].localeCompare(doc.data().endDate) <= 0) {
                        this.state.actualVotes.push({
                            question: doc.data().question,
                            endDate: doc.data().endDate,
                            importance: doc.data().type,
                            infoslide: doc.data().infoslide,
                            id: doc.id,
                        })
                        this.setState({ actualVotes: this.state.actualVotes });
                    }
                })
            })
            .catch(error => console.log(error));
    }

    

    startVote = (item) => {
        //In momentul in care user-ul apasa pe un anumit vot, acesta va naviga la pagina Votes.js, 
        //transmitand de asemenea datele din firebase ale acestui vot
        this.props.navigation.navigate('VoteRT', {
            item: item,
        })
    }

    // createVote = () => {
    //     this.props.navigation.navigate('CreateVoteRT');
    // }

    render() {
        return (
            
                <ScrollView style={styles.root}>
                    <FlatList
                        data={this.state.actualVotes}
                        renderItem={({ item }) => (
                            // <View>
                            //     <Text>{item.question}</Text>
                            //     <Text>{item.endDate}</Text>
                            //     <Text>{`Importanta: ${this.renderImportance(item.importance)}`}</Text>
                            //     <TouchableOpacity onPress={() => this.startVote(item)}>
                            //         <Text>Voteaza!</Text>
                            //     </TouchableOpacity>
                            // </View>
                            <VoteCard item = {item} navigate = {() => this.startVote(item)} />
                        )}
                        keyExtractor={item => item.question}
                    />
                    {/* <ActionButton buttonColor="rgba(231,76,60,1)" onPress={() => this.createVote()}
                    position="right" verticalOrientation="up" style={styles.materialButtonShare} /> */}
                </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1
    },
    text: {
        top: 51.73,
        left: "35.64%",
        color: "#121212",
        position: "absolute",
        fontWeight: "bold",
        lineHeight: 30
    },
    materialCardWithoutImage: {
        top: 134.67,
        width: 310.6,
        height: 320,
        position: "absolute"
    },
    materialButtonShare: {
        top: 589.57,
        left: 283,
        width: 56,
        height: 56,
        position: "absolute"
    }
});