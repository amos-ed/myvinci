import React, {Component} from 'react';
import {Text, View, TextInput, StyleSheet, TouchableOpacity, Alert, Image} from 'react-native';
import firebase from 'react-native-firebase';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import ActionButton from 'react-native-action-button';
import { Button, Card, ListItem, Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';

const announcements = firebase.firestore().collection('announcements');

export class Announcements extends Component {

    constructor(props) {
        super(props);
        this.state = {
            announcementsList: [],
            isLoading: true,
        }
    }

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <Button
                    icon={
                        <Icon
                            name="add"
                            size={25}
                            color="black"
                        />
                    }
                    buttonStyle={{ backgroundColor: 'rgb(179, 1, 1)' }}
                    onPress = {navigation.getParam('createAnnouncement')}
                />
            ),
            headerStyle: {
                backgroundColor: 'rgb(179, 1, 1)',
            },
        }
    }

    componentDidMount() {
        this.searchForAnnouncements();
        this.props.navigation.setParams({ createAnnouncement: this.createAnnouncement });
    }

    searchForAnnouncements = async () => {
        //Iau fiecare document din colectia de anunturi si il adaug intr-un array care urmeaza sa fie 
        //afisat pe ecran
        await announcements.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const date = new Date();
                    //In document vom avea urmatoarele:
                    this.state.announcementsList.push({
                        //Data la care a fost adaugat anuntul
                        dateAdded: doc.data().dateAdded,
                        //Email-ul celui care l-a adaugat
                        email: doc.data().email,
                        //Importanta anuntului
                        importance: doc.data().importance,
                        //Numele celui care l-a adaugat
                        name: doc.data().name,
                        //Textul anuntului
                        post: doc.data().post,
                        //ID-ul documentului din firebase pentru a nu le confunda
                        id: doc.id,
                    })
                    this.state.announcementsList.sort((a, b) => {
                        return -a.dateAdded.toString().localeCompare(b.dateAdded.toString())
                    })
                    this.setState({ announcementsList: this.state.announcementsList, isLoading: false });
                })
                // this.state.announcementsList.sort((a, b) => {
                //     if (a.dateAdded.localecompare(b.dateAdded) <= 0) return -1;
                //     return 1;
                // })
            })
        
    }

    getDate = () => {
        const date = new Date();
        return date.toISOString().substring(0, 16);
    }

    loadingScreen = () => {
       //Cat timp inca anunturile nu sunt incarcate in intregime, nu este afisat nimic pe ecran 
        return (
            <View></View>
        )
    }

    getImportanceStyle = (importance) => {
        //In functie de importanta anuntului, avem nevoie de alte atribute de css
        //Astfel, pentru a usura treaba, returnam direct obiectul din StyleSheet de care
        //avem nevoie, in functie de importanta anuntului
        if (importance === 1) return styles.small;
        if (importance === 2) return styles.medium;
        if (importance === 3) return styles.high;
        if (importance === 4) return styles.urgent;
    }

    createAnnouncement = () => {
        this.props.navigation.navigate('CreateAnnouncementRT');
    }

    render (){
        if (this.state.isLoading === true) return this.loadingScreen();
        return(
            <ScrollView style={styles.container}>
                <FlatList 
                    data = {this.state.announcementsList}
                    renderItem = {({ item }) => (
                        // <View style = {this.getImportanceStyle(item.importance)}>
                        //     <Text>{item.name}</Text>
                        //     <Text>{item.dateAdded}</Text>
                        //     <Text>{item.post}</Text>
                        // </View>
                        // <ListItem 
                        //     key = {item.id}
                        //     title = {`Publicat in data ${item.dateAdded}`}
                        //     subtitle = {item.post}
                        //     // leftAvatar = {<Avatar source = {require('./img/circle-cropped.png')} rounded = {true} />}
                        //     leftAvatar = {{
                        //         source: require('./img/circle-cropped.png'),
                        //         title: item.name
                        //     }}
                        //     // rightTitle = {`Publicat in data ${item.dateAdded}`}
                        // />
                        <ListItem 
                            title = {(
                                <View style = {{ flexDirection: 'row' }}>
                                    <Avatar source = {require('./img/circle-cropped.png')} rounded = {true} /> 
                                    <Text style = {{ textAlignVertical: 'center', fontSize: 16, fontWeight: 'bold' }}>{item.name}</Text>
                                </View>
                            )}
                            subtitle = {(
                                <View>
                                    <Text>Publicat in data de {item.dateAdded}</Text>
                                    <Text style = {this.getImportanceStyle(item.importance)}>{item.post}</Text>
                                </View>
                            )}
                        />
                    )}
                    keyExtractor = {item => item.id}
                />
                {/* <ActionButton buttonColor="rgba(231,76,60,1)" onPress = {() => this.createAnnouncement()} 
                position = "right" verticalOrientation = "up" style = {styles.actionButton} /> */}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    small: {
        color: 'green'
    },
    medium: {
        color: '#e0c01d'
    },
    high: {
        color: '#f5580a'
    },
    urgent: {
        color: 'red',
    }
})