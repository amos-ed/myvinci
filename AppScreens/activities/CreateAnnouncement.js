import React from 'react';
import { Text, View, StyleSheet, TextInput, Alert } from 'react-native';
import { CheckBox, Button } from 'react-native-elements';
import firebase from 'react-native-firebase';
import {UserFunctions} from '../sections/UserFunctions.js';


export class CreateAnnouncement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            announcementText: '',
            importance: 1,
            importance1: true,
            importance2: false,
            importance3: false,
            importance4: false,
        }
    }

    dateToString = () => {
        const date = new Date();
        return date.toISOString().split('T')[0];
    }

    checkIfDataExists = () => {
        if (this.state.announcementText === '') {
            Alert.alert("Introdu te rog textul pentru anunt");
            return false;
        }
        return true;
    }

    createAnnouncement = async () => {
        if (this.checkIfDataExists() === false) return;
        var user = '';
        await UserFunctions.SearchByEmail(firebase.auth().currentUser.email)
            .then(response => {
                user = response;
            })
            .catch(error => console.error(error));
        const finalObj = {
            dateAdded: this.dateToString(),
            email: firebase.auth().currentUser.email,
            name: user,
            importance: this.state.importance,
            post: this.state.announcementText,
        }
        const announcementsRef = firebase.firestore().collection("announcements");
        announcementsRef.add(finalObj)
            .then(() => {
                Alert.alert("Anuntul a fost adaugat cu succes");
                this.props.navigation.navigate('HomepageRT');
            })
            .catch(error => console.log(error));
    }

    render() {
        return (
            <View>
                <TextInput
                    placeholder="Introdu aici anuntul..."
                    multiline={true}
                    onChangeText={(text) => this.setState({ announcementText: text })}
                    value={this.state.announcementText}
                />
                <Text>Alege importanta anuntului</Text>
                <CheckBox
                    title="Scazuta"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.importance1}
                    onPress={() => this.setState({
                        importance1: true,
                        importance2: false,
                        importance3: false,
                        importance4: false,
                        importance: 1,
                    })}
                />


                <CheckBox
                    title="Medie"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.importance2}
                    onPress={() => this.setState({
                        importance1: false,
                        importance2: true,
                        importance3: false,
                        importance4: false,
                        importance: 2,
                    })}
                />

                <CheckBox
                    title="Ridicata"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.importance3}
                    onPress={() => this.setState({
                        importance1: false,
                        importance2: false,
                        importance3: true,
                        importance4: false,
                        importance: 3,
                    })}
                />

                <CheckBox
                    title="Urgenta"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.importance4}
                    onPress={() => this.setState({
                        importance1: false,
                        importance2: false,
                        importance3: false,
                        importance4: true,
                        importance: 4,
                    })}
                />

                <Button title = "Submit" onPress = {() => this.createAnnouncement()} />
            </View>
        )
    }
}