import React, { Component } from 'react';
import { Dimensions, View, Text, StyleSheet, TextInput, Alert, TouchableOpacity, Image } from 'react-native';
import PercentageCircle from 'react-native-percentage-circle';
import { StackNavigator, BottomTabBar } from 'react-navigation';
import { Col, Row, Grid } from "react-native-easy-grid";
import {UserFunctions} from '../sections/UserFunctions.js';
import firebase from 'react-native-firebase';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


const deviceWidth = Dimensions.get('window').width;

export class Homepage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            user: {},
            loading: true,
        };
    }
    static navigationOptions = {

        headerMode: 'screen',
        header: null


    };

    async componentDidMount() {
        const email = firebase.auth().currentUser.email;
        UserFunctions.GetUserDetails(email)
            .then(response => {
                this.setState({ user: response, loading: false });
            })
            .catch(error => console.log(error));
    }

    BDCD_Promote = () => {
        if (this.state.user.userType === "BD/CD") this.props.navigation.navigate('PromoteRT');
    }

    loadingScreen = () => {
        return <View></View>
    }

    logout = () => {
        firebase.auth().signOut();
        this.props.navigation.navigate('LoginRT');
    }

    render() {
        const { navigate } = this.props.navigation;
        if (this.state.loading === false) return (
            <View style={styles.container}>
                <View style={{flex:1}}>
                    <TouchableOpacity onPress = {this.logout} style={styles.signOutBox}>
                        <Icon style={styles.signOut} name="sign-out"></Icon>
                    </TouchableOpacity>
                </View>
                <View style={styles.profileContainer} onTouchStart = {this.BDCD_Promote}>

                    {/* <TouchableOpacity onPress = {() => console.log("Merge")}> */}
                        <PercentageCircle radius={75} percent={50} color={"#b30505"}>
                            <Image style={styles.profilePicture} source={require('./img/circle-cropped.png')}></Image>
                        </PercentageCircle>

                        <View style={styles.infos}>
                            <Text style={{ fontWeight: 'bold' }}>Leonardo Da Vinci</Text>
                            <Text>Nivel: <Text style={{ fontWeight: 'bold', color: '#b30505' }}> 42</Text></Text>
                            <Text style={{ fontStyle: 'italic', color: '#b30505' }}>DaVincer</Text>
                        </View>
                    {/* </TouchableOpacity> */}

                </View>



                <Grid style={styles.menuContainer}>
                    <Col>
                        <TouchableOpacity style={styles.squareFormenu} onPress={() => navigate('ClubsRT')}>
                            <Row >
                                <View style={styles.cerc}><View style={styles.romb}></View></View>
                            </Row>
                        </TouchableOpacity>

                        <Row style={{ height: deviceWidth / 7 }}></Row>
                        <TouchableOpacity style={styles.squareFormenu} onPress={() => navigate('VotesListRT')}>
                            <Row >
                                <Image style={styles.menuPicture} source={require('./img/vote.png')}></Image>
                            </Row>
                        </TouchableOpacity>
                    </Col>

                    <Col>
                        <TouchableOpacity style={styles.squareFormenu} onPress={() => navigate('EventsRT')}>
                            <Row >
                                <Image style={styles.menuPicture} source={require('./img/iconfinder_calendar-check-o_1608596.png')}></Image>
                            </Row>
                        </TouchableOpacity>

                        <Row style={{ height: deviceWidth / 7 }}>
                        </Row>

                        <TouchableOpacity style={styles.squareFormenu} onPress={() => navigate('AnnouncementsRT')}>
                            <Row >
                                <Image style={styles.menuPicture} source={require('./img/megaphone.png')}></Image>
                            </Row>
                        </TouchableOpacity>
                    </Col>
                </Grid>

                {//createMaterialBottomTabNavigator}
                }
            </View>
        )
        else return this.loadingScreen();
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Georgia',
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignContent: 'center',
    },
    infos: {
        flex: 2,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileContainer: {

        flex: 4,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '8%',
    },
    profilePicture: {

        width: '100%',
        width: deviceWidth / 2,
        height: deviceWidth / 2
    },
    menuContainer: {
        flex: 6,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'

    },
    squareFormenu: {

        borderWidth: 4,
        borderRadius: 20,
        height: deviceWidth / 3,
        width: deviceWidth / 3,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',

    },

    menuPicture: {
        height: deviceWidth / 4,
        width: deviceWidth / 4,
        alignSelf: "center"
    },

    cerc: {
        height: deviceWidth / 4,
        width: deviceWidth / 4,
        alignSelf: "center",
        borderRadius: 100,
        borderWidth: 5,
        borderColor: '#b30505',
        alignItems: 'center',
        justifyContent: 'center',
    },

    romb: {
        height: deviceWidth / 7,
        width: deviceWidth / 7,
        borderWidth: 5,
        transform: [
            { rotate: '45deg' }
        ],
        alignSelf: "center",
    },
    submit: {
        textAlign: 'center',
        fontSize: 24,
        fontWeight: 'bold',
        margin: '2%'
    },
    signOutBox:{
        flex:1,
        justifyContent:'center'
    },
    signOut:{
        fontSize:20,
        alignSelf:'flex-end',
        margin:'3%'

    }
})