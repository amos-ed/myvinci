import React from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import firebase from 'react-native-firebase';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import {UserFunctions} from '../sections/UserFunctions';
import ActionButton from 'react-native-action-button';
import EventFunctions from '../sections/EventFunctions.js';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';


const debate = firebase.firestore().collection('activities').doc('WeeklyActivities').collection('DebateClub');
const business = firebase.firestore().collection('activities').doc('WeeklyActivities').collection('BusinessClub');
const Engineering = firebase.firestore().collection('activities').doc('WeeklyActivities').collection('EngineeringClub');
const character = firebase.firestore().collection('activities').doc('WeeklyActivities').collection('CharacterClub');

export class ClubEventList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            club: this.props.navigation.getParam('club'),
            eventList: [],
            loading: true,
            showActionButton: false,
        }
    }

    // static navigationOptions = {
    //     title: "Ceva titlu"
    // }

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <Button
                    icon={
                        <Icon
                            name="add"
                            size={25}
                            color="black"
                        />
                    }
                    buttonStyle={{ backgroundColor: 'rgb(179, 1, 1)' }}
                    onPress = {navigation.getParam('createEvent')}
                />
            ),
            headerStyle: {
                backgroundColor: 'rgb(179, 1, 1)',
            },
        }
    }

    componentDidMount() {
        UserFunctions.GetUserDetails(firebase.auth().currentUser.email)
            .then(response => {
                if (response.userType === "MRP" || response.userType === "JMRP" || response.userType === "BD/CD") this.props.navigation.setParams({ createEvent: this.createEvent });
                else if (
                    (this.state.club === "Character" && (
                        response.userType === "MRU" || response.userType === "JMRU")) ||
                    (this.state.club === "Business" && (
                        response.userType === "MRF" || response.userType === "JMRF")) ||
                    (this.state.club === "Engineering" && (
                        response.userType === "MRD" || response.userType === "JMRD"
                    ))) {
                    this.props.navigation.setParams({ createEvent: this.createEvent });
                }
                else this.props.navigation.setParams({ createEvent: null });
            })
            .catch(error => console.log(error));
        if (this.state.club === "Debate") this.getDebateClub();
        if (this.state.club === "Business") this.getBusinessClub();
        if (this.state.club === "Engineering") this.getEngineeringClub();
        if (this.state.club === "Character") this.getCharacterClub();
        this.setState({ loading: false });
    }

    dateToString = () => {
        const date = new Date();
        return date.toISOString().split('T')[0];
    }

    getDebateClub = () => {
        debate.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const item = doc.data();
                    var shortDescription = item.shortDescription;
                    if (shortDescription.length < item.description.length) shortDescription = shortDescription.concat("...");
                    if (this.dateToString().localeCompare(item.date.toString()) <= 0) {
                        this.state.eventList.push({
                            date: item.date,
                            description: item.description,
                            location: item.location,
                            name: item.name,
                            organizer: item.organizer,
                            going: item.going,
                            password: item.password,
                            id: doc.id,
                            hour: item.hour,
                            shortDescription: shortDescription,
                        })
                    }
                })
                this.state.eventList.sort((a, b) => {
                    return a.date.toString().localeCompare(b.date.toString());
                })
                this.setState({ eventList: this.state.eventList, loading: false });
            })
    }

    getBusinessClub = () => {
        business.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const item = doc.data();
                    var shortDescription = item.shortDescription;
                    if (shortDescription.length < item.description.length) shortDescription = shortDescription.concat("...");
                    if (this.dateToString().localeCompare(item.date.toString()) <= 0) {
                        this.state.eventList.push({
                            date: item.date,
                            description: item.description,
                            location: item.location,
                            name: item.name,
                            organizer: item.organizer,
                            going: item.going,
                            password: item.password,
                            id: doc.id,
                            hour: item.hour,
                            shortDescription: shortDescription,
                        })
                    }
                })
                this.state.eventList.sort((a, b) => {
                    return a.date.toString().localeCompare(b.date.toString());
                })
                this.setState({ eventList: this.state.eventList, loading: false });
            })
    }

    getCharacterClub = () => {
        character.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const item = doc.data();
                    var shortDescription = item.shortDescription;
                    if (shortDescription.length < item.description.length) shortDescription = shortDescription.concat("...");
                    if (this.dateToString().localeCompare(item.date.toString()) <= 0) {
                        this.state.eventList.push({
                            date: item.date,
                            description: item.description,
                            location: item.location,
                            name: item.name,
                            organizer: item.organizer,
                            going: item.going,
                            password: item.password,
                            id: doc.id,
                            hour: item.hour,
                            shortDescription: shortDescription,
                        })
                    }
                })
                this.state.eventList.sort((a, b) => {
                    return a.date.toString().localeCompare(b.date.toString());
                })
                this.setState({ eventList: this.state.eventList, loading: false });
            })
    }

    getEngineeringClub = () => {
        Engineering.get()
            .then(response => {
                response.docs.forEach(doc => {
                    const item = doc.data();
                    var shortDescription = item.shortDescription;
                    if (shortDescription.length < item.description.length) shortDescription = shortDescription.concat("...");
                    if (this.dateToString().localeCompare(item.date.toString()) <= 0) {
                        this.state.eventList.push({
                            date: item.date,
                            description: item.description,
                            location: item.location,
                            name: item.name,
                            organizer: item.organizer,
                            going: item.going,
                            password: item.password,
                            id: doc.id,
                            hour: item.hour,
                            shortDescription: shortDescription,
                        })
                    }
                })
                this.state.eventList.sort((a, b) => {
                    return a.date.toString().localeCompare(b.date.toString());
                })
                this.setState({ eventList: this.state.eventList, loading: false });
            })
    }

    createEvent = () => {
        this.props.navigation.navigate('CreateEventRT', {
            club: this.state.club,
        });
    }

    

    getEventDetails = async (eventID) => {
        var finalEvent = {};
        await this.state.eventList.map(event => {
            if (event.id.localeCompare(eventID) === 0) {
                finalEvent = event;
            }
        })
        return finalEvent;
    }

    navigateToEventPage = async (id) => {
        var event = {};
        await this.getEventDetails(id)
            .then(response => {
                event = response;
            })
            .catch(error => console.log(error));
        this.props.navigation.navigate('EventPageRT', {
            club: this.state.club,
            id: id,
            event: event,
        })
    }

    render() {
        if (this.state.loading === true) {
            return (
                <View></View>
            )
        }
        else {
            return (
                <ScrollView>
                    <FlatList
                        data={this.state.eventList}
                        renderItem={({ item, index }) => (
                            // <View>
                            //     <Text>{item.name}</Text>
                            //     <Text>Evenimentul va avea loc in data de {item.date}</Text>
                            //     <Text>Descrierea evenimentului: {item.shortDescription}</Text>
                            //     <Button title="Mai multe detalii" onPress={() => this.navigateToEventPage(item.id)} />
                            // </View>
                            <Card style = {{ margin: '2%'}}>
                                <CardImage 
                                    source = {require('./img/agdav.png')}
                                    style = {{ backgroundColor: 'white' }}
                                />
                                <CardTitle title = {item.name} />
                                <CardContent text = {`Evenimentul va avea loc in data de ${item.date}`} />
                                <CardContent text = {item.shortDescription} />
                                <CardAction 
                                    separator = {true}
                                >
                                    <CardButton 
                                        onPress = {() => this.navigateToEventPage(item.id)}
                                        title = "Mai multe detalii"
                                        color = "blue"
                                    />
                                </CardAction>
                            </Card>
                        )}
                        keyExtractor={item => item.id}
                    />
                </ScrollView>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    actionButton: {
        position: 'absolute',
        bottom: 0,
        right: 0,
    },
})