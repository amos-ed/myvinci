import React from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { CheckBox, Button } from 'react-native-elements';
import DateTimePicker from 'react-native-modal-datetime-picker';
import firebase from 'react-native-firebase';
import {UserFunctions} from '../sections/UserFunctions.js';
import generator from 'generate-password';

var ref = firebase.firestore().collection('activities');

export class CreateEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isBigEvent: false,
            date: '',
            description: '',
            location: '',
            name: '',
            organizer: {},
            longEvent: false,
            hour: '',
            isPickerVisible: false,
            password: '',
            dates: [],
            chooseDate: "Alege data activitatii",
            club: this.props.navigation.getParam('club'),
        }
        var password = generator.generate({
            length: 10,
            numbers: true,
        })
        console.log(password);
    }

    getClub = () => {
        const club = this.state.club;
        if (club === "Debate") {
            return ref.doc('WeeklyActivities').collection('DebateClub');
        }
        if (club === "Business") {
            return ref.doc('WeeklyActivities').collection('BusinessClub');
        }
        if (club === "Character") {
            return ref.doc('WeeklyActivities').collection('CharacterClub');
        }
        if (club === "Engineering") {
            return ref.doc('WeeklyActivities').collection('EngineeringClub');
        }
    }

    create = () => {
        if (this.state.isBigEvent) this.createBigEvent();
        else {
            this.getClub();
            this.createEvent();
        }
    }

    // createBigEvent = () => {
        
    // }

    dataExists = () => {
        if (this.state.password === '') {
            var password = generator.generate({
                numbers: true,
                length: 10,
            })
            this.setState({password: password});
        }
        if (this.state.name === '') {
            Alert.alert("Introdu te rog numele activitatii");
            return false;
        }
        if (this.state.date === '') {
            Alert.alert("Introdu te rog data si ora la care va avea loc activitatea");
            return false;
        }
        if (this.state.description === '') {
            Alert.alert("Introdu te rog o descriere a activitatii");
            return false;
        }
        if (this.state.location === '') {
            Alert.alert("Introdu te rog locatia unde va avea loc activitatea");
            return false;
        }
        return true;
    }

    createEvent = async () => {
        if (this.dataExists() === false) return;
        var organizer = {};
        await UserFunctions.SearchByEmail(firebase.auth().currentUser.email)
            .then(response => {
                organizer = {
                    email: firebase.auth().currentUser.email,
                    name: response,
                }
            })
            .catch(error => console.error(error));
        const finalObj = {
            date: this.state.longEvent ? this.state.dates : this.state.date,
            description: this.state.description,
            organizer: organizer,
            location: this.state.location,
            name: this.state.name,
            hour: this.state.hour,
            shortDescription: this.state.description.substr(0, 500),
            password: this.state.password,
        }
        const clubRef = this.getClub();
        clubRef.add(finalObj)
            .then(() => {
                this.props.navigation.navigate('HomepageRT');
            })
            .catch(error => console.error(error));
    }

    dateToString = (date) => {
        return date.toISOString().split('T')[0];
    }

    showDateTimePicker = () => {
        this.setState({ isPickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isPickerVisible: false });
    };

    handleDatePicked = date => {
        this.setState({date: this.dateToString(date), hour: date.toString().substring(16, 21)});
        if (this.state.longEvent) this.state.dates.push({
            date: this.state.date,
            hour: this.state.hour,
        });
        this.hideDateTimePicker();
    };

    getDateAndHourIfExists = () => {
        if (this.state.date !== '' && this.state.hour !== '') {
            return `Activitatea va avea loc in data ${this.state.date} de la ora ${this.state.hour}`;
        }
        else return '';
    }

    render() {
        return (
            <ScrollView>
                <TextInput
                    placeholder="Numele activitatii..."
                    onChangeText={(text) => this.setState({ name: text })}
                    value={this.state.name} />
                {/* <CheckBox
                    title='Eveniment'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.isBigEvent}
                    onPress={() => this.setState({ isBigEvent: !this.state.isBigEvent })}
                />
                <CheckBox
                    title="Activitate normala"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={!this.state.isBigEvent}
                    onPress={() => this.setState({ isBigEvent: !this.state.isBigEvent })}
                /> */}
                {/* <CheckBox
                    title="Eveniment pe mai multe zile"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.longEvent}
                    onPress={() => this.setState(prevState => ({
                        longEvent: !prevState.longEvent,
                        chooseDate: !prevState.longEvent ? "Alege pe rand datele in care se va desfasura activitatea" : "Alege data activitatii",
                        dates: [],
                    }))}
                /> */}
                <TextInput
                    multiline={true}
                    placeholder="Descrierea activitatii..."
                    onChangeText={(text) => this.setState({ description: text })}
                    value={this.state.description}
                />
                <TextInput
                    placeholder="Locatia activitatii..."
                    onChangeText={(text) => this.setState({ location: text })}
                    value={this.state.location}
                />
                <TouchableOpacity onPress={() => this.showDateTimePicker()}>
                    <Text style={{ fontSize: 24, fontWeight: 'bold' }}>{this.state.chooseDate}</Text>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isPickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    mode = "datetime"
                />
                <Text>{this.getDateAndHourIfExists()}</Text>
                <TextInput 
                    placeholder = "Adauga o parola pentru eveniment (optional)"
                    onChangeText = {(text) => this.setState({password: text})}
                    value = {this.state.password}
                    secureTextEntry = {true}
                />
                <Button title = "Submit" onPress = {() => this.createEvent()}/>
            </ScrollView>
        )
    }
}