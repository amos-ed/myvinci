import React, {Component} from 'react';
import {Dimensions,Text, View, TextInput, StyleSheet, TouchableOpacity, Alert,Image} from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";

const deviceWidth = Dimensions.get('window').width;
export class Clubs extends Component {

    static navigationOptions = {
        
        title:' ',
        headerStyle: {
            backgroundColor: '#b30505',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            color:"white",
            alignSelf: 'center' ,
            textAlign: 'center',
            
           
            
          },
         
        
      };

      navigateToClubPage = (club) => {
          this.props.navigation.navigate('ClubEventListRT', {
              club: club
          });
      }



    render (){
        return(
            <View style={styles.container}>
                <Grid style={{flex:10}}> 
                    <Row style={{height:deviceWidth/10,alignContent:'center', justifyContent:'center',alignItems:'center'}} >
                        <Text style={styles.clubText}>
                            Alege un club
                        </Text>
                    </Row>
                    <Row >
                        
                            <Col  style={styles.clubContainer}>
                                    <TouchableOpacity style={{flex:1, justifyContent:'center'}}
                                    onPress = {() => this.navigateToClubPage("Debate")}>
                                        <Image style={styles.clubPicture} source={require('./img/clubs/debate.png')}></Image>
                                        <Text style={styles.clubText}>Debate</Text>
                                    </TouchableOpacity>
                            </Col>


                        
                            <Col style={styles.clubContainer}>
                                    <TouchableOpacity style={{flex:1, justifyContent:'center'}}
                                    onPress = {() => this.navigateToClubPage("Business")}> 
                                        <Image style={styles.clubPicture} source={require('./img/clubs/business.png')}></Image>
                                        <Text style={styles.clubText}>Business</Text>
                                    </TouchableOpacity>
                            </Col>
                      
                    </Row>


                    <Row >
                        
                        <Col style={styles.clubContainer}>
                                    <TouchableOpacity style={{flex:1}} 
                                    onPress = {() => this.navigateToClubPage("Character")}>
                                        
                                        <Image style={styles.clubPicture} source={require('./img/clubs/character.png')}></Image>
                                        <Text style={styles.clubText}>Character</Text>
                                    </TouchableOpacity>
                        </Col>




                        <Col style={styles.clubContainer} >
                                    <TouchableOpacity style={{flex:1}} onPress = {() => this.navigateToClubPage("Engineering")}>
                                        <Image style={styles.clubPicture} source={require('./img/clubs/numbers.png')}></Image>
                                        <Text style={styles.clubText}>Engineering</Text>
                                    </TouchableOpacity>
                        </Col>
                    


                    
                       
                  
                </Row>

                </Grid>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },

    clubPicture: {
        alignSelf:"center",
        width:deviceWidth/2,
        height:deviceWidth/2,
    },
    clubContainer: {
        alignContent:'center',
        justifyContent:'center',
        alignItems:'center'
    },

    clubText: {
        fontWeight:'bold',
        color:'black',
        alignSelf:'center',
        fontSize:18
    }
})