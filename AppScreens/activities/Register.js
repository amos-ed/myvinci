import React, {Component} from 'react';
import {Text, View, TextInput, StyleSheet, TouchableOpacity, Alert, Image
} from 'react-native';
import firebase from 'react-native-firebase';
import * as EmailValidator from 'email-validator';

// const userTypes = firebase.firestore().collection('userTypes');
// const usernames = firebase.firestore().collection('usernames');
const users = firebase.firestore().collection('users');

export class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            firstName: '',
            lastName: '',
        }
    }

    registerDavincer = () => {
        //Facem o verificare manuala a datelor pentru a fi siguri ca user-ul si-a introdus datele
        if (this.state.email === '') Alert.alert("Introdu un email");
        else if (EmailValidator.validate(this.state.email) === false) Alert.alert("Acest email nu este valid");
        else if (this.state.password === '') Alert.alert("Introdu o parola");
        else if (this.state.confirmPassword === '') Alert.alert("Confirma parola");
        else if (this.state.password !== this.state.confirmPassword) Alert.alert("Parolele nu coincid");
        else if (this.state.password.length < 6) Alert.alert("Parola introdusa este prea scurta. Aceasta trebuie sa contina minim 6 caractere");
        else {
            //Daca totul e ok, atunci facem un request la firebase pentru a crea user-ul
            //cu email-ul si parola introduse
            firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
                .then(() => {
                    users.add({
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        email: this.state.email,
                        level: 1,
                        xp: 0,
                        userType: "Membru asociat",
                    })
                    .then(() => {
                        this.props.navigation.navigate('HomepageRT');
                    })
                    .catch(error => console.log(error));
                })
                .catch(error => console.log(error));
        }
    }

    render() {
        return (
            <View style = {styles.container}>
                <View style={styles.credentials}>
                    <View style={styles.credentialContainer}>
                        <TextInput 
                            style={styles.credential}
                            value = {this.state.email}
                            placeholder = "Introdu email-ul..."
                            onChangeText = {(text) => this.setState({ email: text })}
                            keyboardType = "email-address"
                            autoCapitalize = {true}
                        />
                    </View>

                    <View style={styles.credentialContainer}>
                        <TextInput 
                            style={styles.credential}
                            value = {this.state.password}
                            placeholder = "Introdu parola..."
                            onChangeText = {(text) => this.setState({ password: text })}
                            secureTextEntry = {true}
                            autoCapitalize = {true}
                        />
                    </View>
                    
                    <View style={styles.credentialContainer}>
                        <TextInput 
                            style={styles.credential}
                            value = {this.state.confirmPassword}
                            placeholder = "Confirmă parola..."
                            onChangeText = {(text) => this.setState({ confirmPassword: text })}
                            secureTextEntry = {true}
                            autoCapitalize = {true}
                        />
                    </View>

                    <View style={styles.credentialContainer}>    
                        <TextInput 
                            style={styles.credential}
                            value = {this.state.lastName}
                            placeholder = "Numele tău..."
                            onChangeText = {(text) => this.setState({lastName: text})}
                            autoCapitalize = {false}
                        />
                    </View>   

                    <View style={styles.credentialContainer}>
                        <TextInput 
                            style={styles.credential}
                            value = {this.state.firstName}
                            placeholder = "Prenumele tău..."
                            onChangeText = {(text) => this.setState({firstName: text})}
                            autoCapitalize = {false}
                        />
                     </View>   
                    </View>
                <View style={styles.buttons}>
                <TouchableOpacity style={styles.button1} onPress = {() => this.registerDavincer()}>
                    <Text style = {styles.submit}>Creează contul</Text>
                </TouchableOpacity>
                </View>

                <View style={styles.logoBox}>
                    <Image  source={require('./img/Logo.png')} style={styles.logo}></Image>
                </View>
            </View>
        
        )
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Georgia',
        flex: 1,
        backgroundColor: 'white',
        textAlign: 'center',
        justifyContent:'center'
        
    },
    credentialContainer:{
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'center'
    },
    credentials: {
        flex:5,
        justifyContent:'center',
        textAlign:'center'
    },
    credential: {
       marginLeft:'3%',
       marginTop:'2%',
       borderBottomColor:'gray',
       borderBottomWidth: 1,
      textAlign: 'center',
       width:'80%'
     
    },
    submit: {
        textAlign: 'center',
         
        color:'white',
        textTransform:'uppercase',
        fontWeight:'bold',
    },
    buttons: {
        flex:1,
        alignContent:'center'
    },
    button1:{
        backgroundColor:"#b30505",
        flex:1,
        justifyContent:'center',
        margin:'1%',
        textAlign:'center'
    },
    logoBox: {
        flex:1,
        justifyContent:"center", 
        alignContent:'center',
        alignItems:'center' 
       },
       logo:{
           height:50,
           width:50
       }

})