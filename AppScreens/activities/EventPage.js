import React from 'react';
import { View, Text, StyleSheet, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { Button } from 'react-native-elements';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';
import { ScrollView } from 'react-native-gesture-handler';


export class EventPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.navigation.getParam('event'),
            club: this.props.navigation.getParam('club'),
        }
    }

    canCheckIn = () => {
        //User-ul logat nu va vedea butonul de check in daca:
        //1. Este organizatorul activitatii
        //2. Daca este deja in lista de check in
        if (this.state.item.organizer.email === firebase.auth().currentUser.email) return false;
        //Daca lista nu exista, atunci va putea sa isi de check in
        if (this.state.item.going === undefined || this.state.item.going === null) return true;
        else {
            var returnValue = true;
            this.state.item.going.map(user => {
                if (user.email.localeCompare(firebase.auth().currentUser.email.toString()) === 0) returnValue = false;
            });
            return returnValue;
        }
    }

    showEventPassword = () => {
        Alert.alert(this.state.item.password);
    }

    isOrganizer = () => {
        //Verific daca user-ul logat este sau nu organizator al activitatii
        //Daca da, atunci el va putea vedea parola activitatii si o va putea inregistra
        if (this.state.item.organizer.email === firebase.auth().currentUser.email) {
            return <Button title="Vezi codul evenimentului" onPress={this.showEventPassword} />
        }
        return null;
    }

    //Formatez data
    dateToString = (date) => {
        return date.toISOString().split('T')[0];
    }

    isHourReady = () => {
        //Verific daca ora actuala este cuprinsa in intervalul [hour - 1, hour + 2] si in 
        //functie de asta, returnez true sau false
        const date = new Date();
        const d = this.state.item.date;
        const hour = parseInt(this.state.item.hour.substring(0, 2));
        if (this.dateToString(date) === d && (hour === date.getHours() - 1
            || (hour <= date.getHours() && hour + 2 >= date.getHours()))) {
            return true;
        }
        return false;
    }

    renderCheckIn = () => {
        //Daca user-ul poate sa isi dea check in si ora este cuprinsa in intervalul de mai sus, 
        //atunci afisez butonul de check in
        if (this.canCheckIn() === true && this.isHourReady() === true) {
            return <Button title="Check in" onPress={this.checkIn} />
        }
        else return null;
    }

    checkIn = () => {
        //Navighez la pagina unde trebuie introdusa parola activitatii pentru a fi inregistrat
        const password = this.props.navigation.getParam("password");
        this.props.navigation.navigate('CheckInRT', {
            id: this.state.item.id,
            club: this.state.club
        });
    }

    render() {
        const item = this.state.item;
        return (
            // <View>
            //     <Text>{item.name}</Text>
            //     <Text>{item.description}</Text>
            //     <Text>{item.location}</Text>
            //     <Text>{item.date}</Text>
            //     <Text>{item.organizer.name}</Text>
            //     {this.renderCheckIn()}
            //     {this.isOrganizer()}
            // </View>
            <ScrollView>
                <Card style={{ margin: '2%' }}>
                    <CardImage
                        source={require('./img/agdav.png')}
                        style={{ backgroundColor: 'white' }}
                    />
                    <CardTitle title={item.name} />
                    <CardContent text={`Evenimentul va avea loc in data de ${item.date}`} />
                    <CardContent text={`Acesta se va desfasura la ${item.location}`} />
                    <CardContent text={item.description} />
                    <CardContent text={`Pentru mai multe detalii, puteti apela la ${item.organizer.name}`} />
                    {/* <CardAction>
                    {this.renderCheckIn()}
                    {this.isOrganizer()}
                </CardAction> */}
                    <View>
                        {this.renderCheckIn()}
                        {this.isOrganizer()}
                    </View>
                </Card>
            </ScrollView>
        )
    }
}