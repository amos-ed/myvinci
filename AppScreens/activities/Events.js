import React, { Component } from 'react';
import { Text, View, TextInput, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import firebase from 'react-native-firebase';
import { FlatList } from 'react-native-gesture-handler';
import EventFunctions from '../sections/EventFunctions';

const events = firebase.firestore().collection('activities');
const business = events.doc('WeeklyActivities').collection('BusinessClub');
const numbers = events.doc('WeeklyActivities').collection('EngineeringClub');
const character = events.doc('WeeklyActivities').collection('CharacterClub');
const debate = events.doc('WeeklyActivities').collection('DebateClub');

export class Events extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {},
    };
  }


  componentDidMount() {
    this.initializeBigEvents();
    this.initializerNumbersClub();
    this.initializerBusinessClub();
    this.initializerCharacterClub();
    this.initializerDebateClub();
  }

  initializeBigEvents = () => {
    events.get()
      .then(response => {
        response.docs.forEach(doc => {
          if (doc.id !== 'WeeklyActivities') {
            //Evenimentele mari sunt cele care nu au colectie legata de documentul WeeklyActivities
            const i = doc.data();
            if (i.longEvent) {
              //In cazul in care avem un eveniment lung (pe mai multe zile), atunci vom lua toate zilele acelui eveniment
              i.date.map(date => {
                if (!this.state.items[date]) this.state.items[date] = [];
                this.state.items[date].push({
                  description: i.description,
                  location: i.location,
                  name: i.name,
                  id: doc.id,
                  organizer: i.organizer,
                  type: 'Event',
                })
              })
            }
            else {
              //In cazul in care evenimentul este pe o singura zi, luam pur si simplu data
              if (!this.state.items[i.date]) this.state.items[i.date] = [];
              this.state.items[i.date].push({
                description: i.description,
                location: i.location,
                name: i.name,
                organizer: i.organizer,
                type: 'Event',
              })
            }
          }
        })
      })
  }

  initializerBusinessClub = () => {
    //Cauta in colectia "Business Club"
    business.get()
      .then(response => {
        //Iau documentele colectiei "Business Club"
        const item = response.docs;
        item.forEach(doc => {
          const i = doc.data();
          var shortDescription = i.shortDescription;
          if (shortDescription.toString().length !== i.description.toString().length) {
            shortDescription = shortDescription.toString().concat('...');
          }
          //Verific fiecare document daca este nedefinit sau null
          //Daca e, atunci il setez ca vector gol pentru a putea sa afisez card-ul pentru zi fara evenimente
          if (!this.state.items[i.date]) this.state.items[i.date] = [];
          //Stiind ca pe pozitia i.date din map avem un array, putem linistiti sa adaugam urmatoarele elemente:
          this.state.items[i.date].push({
            //Descrierea evenimentului
            description: i.description,
            //Descrierea scurtata a acestuia
            shortDescription: shortDescription,
            //Locatia evenimentului
            location: i.location,
            //Numele evenimentului
            name: i.name,
            //ID-ul evenimentului pentru a putea sa il accesam in firebase
            id: doc.id,
            //Datele organizatorului
            organizer: i.organizer,
            //Tipul de activitate
            type: 'Normal',
            //Tipul activitatii va fi folosit pentru a randa altfel un eveniment fata de o 
            //activitate normala
            club: "Business",
            //Definesc clubul pentru a putea cauta documentul doar in colectia clubului
            password: i.password,
            //Parola evenimentului pentru ca oamenii sa poata sa isi dea check in
          })
          //Apelam setState pentru a se reincarca pagina
          this.setState({
            items: this.state.items
          })
        })
      })
      .catch(error => console.log(error));
  }

  initializerDebateClub = () => {
    //Lucrurile stau la fel ca si la Business Club
    debate.get()
      .then(response => {
        const item = response.docs;
        item.forEach(doc => {
          const i = doc.data();
          var shortDescription = i.shortDescription;
          if (shortDescription.toString().length !== i.description.toString().length) {
            shortDescription = shortDescription.toString().concat('...');
          }
          if (!this.state.items[i.date]) this.state.items[i.date] = [];
          this.state.items[i.date].push({
            description: i.description,
            location: i.location,
            name: i.name,
            shortDescription: shortDescription,
            organizer: i.organizer,
            id: doc.id,
            type: 'Normal',
            club: "Debate",
            password: i.password,
          })
          this.setState({
            items: this.state.items
          })
        })
      })
      .catch(error => console.log(error));
  }

  initializerCharacterClub = () => {
    //Lucrurile stau la fel ca si la Business Club
    character.get()
      .then(response => {
        const item = response.docs;
        item.forEach(doc => {
          const i = doc.data();
          var shortDescription = i.shortDescription;
          if (shortDescription.toString().length !== i.description.toString().length) {
            shortDescription = shortDescription.toString().concat('...');
          }
          if (!this.state.items[i.date]) this.state.items[i.date] = [];
          this.state.items[i.date].push({
            description: i.description,
            location: i.location,
            shortDescription: shortDescription,
            name: i.name,
            organizer: i.organizer,
            id: doc.id,
            type: 'Normal',
            password: i.password,
            club: "Character"
          })
          this.setState({
            items: this.state.items
          })
        })
      })
      .catch(error => console.log(error));
  }


  initializerNumbersClub = () => {
    //Lucrurile stau la fel ca si la Business Club
    numbers.get()
      .then(response => {
        const item = response.docs;
        item.forEach(doc => {
          const i = doc.data();
          var shortDescription = i.shortDescription;
          if (shortDescription.toString().length !== i.description.toString().length) {
            shortDescription = shortDescription.toString().concat('...');
          }
          if (!this.state.items[i.date]) this.state.items[i.date] = [];
          this.state.items[i.date].push({
            description: i.description,
            shortDescription: shortDescription,
            location: i.location,
            name: i.name,
            organizer: i.organizer,
            id: doc.id,
            type: 'Normal',
            club: "Digital",
            password: i.password,
          })
          this.setState({
            items: this.state.items
          })
        })
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <Agenda
        items={this.state.items}
        loadItemsForMonth={this.loadItems.bind(this)}
        selected={this.dateToString()}
        renderItem={(item) => this.renderItem(item)}
        renderEmptyDate={() => this.renderEmptyDate()}
        rowHasChanged={this.rowHasChanged.bind(this)} //In momentul in care user-ul se muta la o alta
      //data din calendar, functia rowHasChanged returneaza true, astfel incat calendarul sa poata
      //sa isi modifice pozitia selectata
      />
    );
  }

  loadItems(day) {
    //In momentul in care calendarul incepe sa isi incarce componentele, aceasta functie
    //ia fiecare zi din calendar si o seteaza la un array gol, pentru a evita diverse erori
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        if (!this.state.items[strTime]) {
          this.state.items[strTime] = [];
        }
      }
      const newItems = {};
      Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
      this.setState({
        items: newItems
      });
    }, 2500);
  }

  navigateToEventPage = async (id, club, type) => {
    if (type === "Normal") {
      //Pentru a naviga la pagina evenimentului, avem nevoie de toate informatiile despre activitatea respectiva
      await EventFunctions.GetEventByID(id, club)
        .then(response => {
          this.props.navigation.navigate('EventPageRT', {
            event: response,
            club: club,
            id: id,
          })
        })
        .catch(error => console.log(error));
    }
    else {
      await EventFunctions.GetBigEvent(name) 
        .then(response => {
          this.props.navigation.navigate('EventPageRT', {
            event: response,
          })
        })
        .catch(error => console.log(error));
    }
  }

  renderItem(item) {
    //Aici avem design-ul pentru un card al unui eveniment
    //Cand functia primeste un eveniment, aceasta returneaza un card in care apar urmatoarele:
    return (
      <TouchableOpacity style={[styles.item, { height: item.height }]} onPress = {() => this.navigateToEventPage(item.id, item.club, item.type)}>
        {/* Numele activitatii */}
        <Text>Activitate: {item.name}</Text>
        {/* Locatia activitatii */}
        <Text>Locatie: {item.location}</Text>
        {/* Descrierea activitatii */}
        {/* <Text>Descrierea proiectului: {item.description}</Text> */}
        {/* Datele organizatorului */}
        <Text>Organizator: {item.organizer.name}</Text>
        <Text>Descriere: {item.shortDescription}</Text>
      </TouchableOpacity>
    );
  }

  renderEmptyDate() {
    //In cazul in care nu avem nicio activitate intr-o anumita zi, se afiseaza textul de mai jos
    return (
      <View style={styles.emptyDate}><Text>Nu exista activitati in aceasta zi</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }

  dateToString = () => {
    const date = new Date();
    return date.toISOString().split('T')[0];
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30
  }
});