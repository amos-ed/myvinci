import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList, Alert} from 'react-native';
import firebase from 'react-native-firebase';
import PromoteUser from '../sections/CD&BD_User_Promote';

const userTypes = firebase.firestore().collection('users');

export class Promote extends Component {
    constructor(props) {
        super(props);
        this.unsubscribe = null;
        this.state = {
            userList: [],
            loading: true,
        }
    }

    updateUserList = (querySnapshot) => {
        querySnapshot.forEach((doc) => {
            const document = doc.data();
            this.state.userList.push({
                email: document.email,
                firstName: document.firstName,
                lastName: document.lastName,
                userType: document.userType,
                level: document.level,
                xp: document.xp,
                id: doc.id,
            }) 
            this.setState({userList: this.state.userList, loading: false});
        });
    }

    componentDidMount() {
        userTypes.get()
            .then((response) => {
                this.updateUserList(response);
            })
            .catch(error => Alert.alert(error.toString()));
    }

    render() {
        if (this.state.loading === true) return null;
        return (
            <View>
                <FlatList 
                    data = {this.state.userList}
                    renderItem = {({ item, index }) => (
                        <PromoteUser user = {item} />
                    )}
                    keyExtractor = {item => item.user}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    }
})