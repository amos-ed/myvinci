import React, { Component } from 'react';
import { Text, View, TextInput, StyleSheet, TouchableOpacity, Alert, Image, TouchableHighlight } from 'react-native';
import NoVote from '../sections/NoVote.js';
import firebase from 'react-native-firebase';
import {UserFunctions} from '../sections/UserFunctions.js';
import { Center } from '@builderx/utils';
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/MaterialIcons';
import VoteAnswers from '../sections/VoteAnswers.js';

export class Vote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: {},
            user: '',
            document: {},
            canVote: true,
            loading: true,
            userDetails: {},
        }
    }

    static navigationOptions = {
        headerStyle: {
            backgroundColor: 'rgb(179, 1, 1)',
        },
    }

    canVote = async (doc, user) => {
        //Functia aceasta verifica daca user-ul logat a votat sau nu
        const vote = firebase.firestore().collection('votes').doc(doc);
        await vote.get()
            .then(response => {
                response.data().answers.map(element => {
                    //In momentul in care am gasit un element in array-ul de voturi care corespunde cu
                    //user-ul logat, setam variabila canVote la false pentru a nu il lasa sa voteze inca o data
                    if (element.email === user) this.setState({ canVote: false });
                })
            })
            .catch(error => console.log(error));
    }

    async componentDidMount() {
        const user = firebase.auth().currentUser;
        //In variabila item din state setam parametrul 'item' pe care l-am transmis in componentul VotesList.js
        //Dupa ce il avem pe acesta, putem cauta in firebase direct documentul cu votul care ne intereseaza
        await this.setState({ item: this.props.navigation.getParam('item') });
        await this.canVote(this.state.item.id, user.email);
        await this.getUser(firebase.auth().currentUser.email);
        this.setState({ loading: false });
    }

    getUser = async (userEmail) => {
        //Cautam user-ul logat dupa email, pentru a avea acces la numele lui complet 
        await UserFunctions.SearchByEmail(userEmail)
            .then(response => {
                this.setState({ user: response })
            })
            .catch(error => console.log(error));
        //Obtinem detaliile user-ului ca sa ne dam seama daca putem sa ii afisam lista de voturi
        await UserFunctions.GetUserDetails(userEmail)
            .then(response => {
                this.setState({ userDetails: response, user: response.firstName + " " + response.lastName });
            })
            .catch(error => console.log(error));
    }

    getVotes = async () => {
        //Dupa ce avem setat in state id-ul votului care ne intereseaza, putem sa il cautam in firebase
        //si sa obtinem toate datele acestuia de care avem nevoie sa le afisam pe ecran
        const votes = firebase.firestore().collection('votes');
        let votesList = [];
        await votes.get()
            .then(response => {
                response.docs.forEach(doc => {
                    if (doc.id === this.state.item.id) {
                        votesList = doc.data().answers;
                        this.setState({ document: doc.data() });
                    }
                })
            })
            .catch(error => console.log(error));
        return votesList;
    }

    addVote = async (vote) => {
        //Functia aceasta adauga votul in array-ul "answers" din firebase
        //Initializam un array gol in care adaugam tot ce exista in firebase pana acum
        let votesList = [];
        await this.getVotes()
            .then(response => {
                if (response === null || response === undefined) votesList = [];
                else votesList = response;
            })
            .catch(error => console.log(error));
        //Adaugam ceea ce a votat user-ul actual
        votesList.push({
            email: user.email,
            name: this.state.user,
            answer: vote,
        })
        //In this.state.document avem o copie a documentului din firebase
        //Actualizam array-ul pentru raspunsuri din copia documentului
        this.state.document.answers = votesList;
        const votes = firebase.firestore().collection('votes').doc(this.state.item.id);
        //Actualizam documentul din firebase
        votes.set(this.state.document);
        this.props.navigation.navigate('HomepageRT');
    }

    voted = () => {
        //In cazul in care cineva din BD/CD sau initiatorul votului a votat, poate sa vada rezultatele acestuia
        const user = this.state.userDetails.userType;
        if (user === "BD/CD" || this.state.item.email === firebase.auth().currentUser.email) {
            return <VoteAnswers vote = {this.state.item.id} />
        }
        //In caz ca user-ul logat a votat deja, ii va aparea acest component
        return (
            <View>
                <Text>Ti-ai exprimat parerea deja in acest vot</Text>
            </View>
        )
    }

    loading = () => {
        //Cat timp pagina se incarca, nu va fi afisat nimic
        return (
            <View>

            </View>
        )
    }

    render() {
        if (this.state.loading === true) {
            return this.loading();
        }
        if (this.state.canVote === true) {
            return (
                <View style={styles.container}>

                    <Center vertical>
                        <View style={styles.image}>
                            <Button
                                icon={
                                    <Icon
                                        name="check"
                                        size={35}
                                        color="green"
                                    />
                                }
                                title="Pentru"
                                buttonStyle={{ backgroundColor: 'white' }}
                                titleStyle={{ color: 'black' }}
                                onPress = {() => this.addVote("Pentru")}
                            />
                        </View>
                    </Center>

                    <Center horizontal>
                        <View style={styles.image3}>
                            <Button
                                icon={
                                    <Icon
                                        name="lens"
                                        size={35}
                                        color="black"
                                    />
                                }
                                title="Abtinere"
                                buttonStyle={{ backgroundColor: 'white' }}
                                titleStyle={{ color: 'black' }}
                                onPress = {() => this.addVote("Abtinere")}
                            />

                        </View>
                    </Center>

                    <View style={styles.image2}>
                        <Button
                            icon={
                                <Icon
                                    name="block"
                                    size={35}
                                    color="red"
                                />
                            }
                            title="Impotriva"
                            buttonStyle={{ backgroundColor: 'white' }}
                            titleStyle={{ color: 'black' }}
                            onPress = {() => this.addVote("Impotriva")}
                        />
                    </View>


                    <Text style={styles.text4}>Voteaza!</Text>
                    <Center horizontal>
                        <View style={styles.text5}>
                            <Text style={{ textAlign: 'center', fontSize: 18, fontWeight: 'bold' }}>{this.state.item.question}</Text>
                        </View>
                    </Center>
                    <Center horizontal>
                        <Text style={styles.text6}>{this.state.item.infoslide}</Text>
                    </Center>
                    {/* </TouchableOpacity>
                    </View> */}
                </View>
            )
        }
        else return this.voted();
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: "rgb(255,255,255)"
    },
    image: {
        left: 19.68,
        top: 330,
        // top: '800%',
        width: 100,
        height: 100,
        position: "absolute",
        alignItems: 'center'
        // borderColor: "#000000",
        // borderWidth: 4
    },
    image2: {
        left: 260.5,
        width: 100,
        height: 100,
        top: 330,
        position: "absolute",
        alignItems: 'center'
        // borderColor: "#000000",
        // borderWidth: 4
    },
    image3: {
        top: 494.02,
        width: 92.44,
        height: 86.39,
        position: "absolute"
    },
    view3: {

    },
    text: {
        top: 439.84,
        left: 44.97,
        color: "#121212",
        position: "absolute"
    },
    text2: {
        top: 441.35,
        left: 265.78,
        color: "#121212",
        position: "absolute"
    },
    text3: {
        top: 601.67,
        color: "#121212",
        position: "absolute"
    },
    text4: {
        top: 73.83,
        left: "39.82%",
        color: "#121212",
        position: "absolute",
        fontSize: 21,
        fontWeight: "bold",
        lineHeight: 20
    },
    text5: {
        top: 104.08,
        position: "absolute",
        fontSize: 14
    },
    text6: {
        top: 167.6,
        color: "#121212",
        position: "absolute"
    }
});