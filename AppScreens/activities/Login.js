import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Alert, TouchableOpacity,Image } from 'react-native';
import * as EmailValidator from 'email-validator';
import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };
    }

    login = () => {
        //Fac o verificare manuala a datelor pentru a fi siguri ca user-ul si-a introdus tot ce trebuie
        if (this.state.email === '') Alert.alert("Introdu email-ul tau");
        else if (EmailValidator.validate(this.state.email) === false) Alert.alert("Adresa de email nu este valida");
        else if (this.state.password === '') Alert.alert("Introdu parola");
        else {
            //Daca totul e ok, facem un request la firebase
            //pentru a loga user-ul cu email-ul si parola introduse
            firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
                .then((response) => {
                    this.props.navigation.navigate('HomepageRT');
                })
                .catch((error) => {
                    Alert.alert("Contul nu a fost gasit");
                })
        }
    }

    render() {
        return (
            <View style={styles.container}>
            <View style={styles.credentials}>
                <View style={styles.credentialContainer}>
                    <Icon name="user" style={{textAlign:'auto'}}></Icon>
                    <TextInput
                    style={styles.credential}
                        value={this.state.email}
                        placeholder="Email..."
                        onChangeText={(text) => this.setState({ email: text })}
                        keyboardType="email-address"
                        autoCapitalize={false}
                    />
                </View>

                <View style={styles.credentialContainer}>
                <Icon name="lock"></Icon>
                <TextInput
                
                  style={styles.credential}
                    value={this.state.password}
                    placeholder="Parola..."
                    onChangeText={(text) => this.setState({ password: text })}
                    secureTextEntry={true}
                    autoCapitalize={false}
                />
                  </View>
            </View>
                <View style={styles.buttons}>
                    <TouchableOpacity onPress={() => this.login()} style={styles.button1}>
                        <Text style={styles.submit}>Intră în cont</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('RegisterRT')} style={styles.button2}>
                        <Text style={styles.submit}>Creează un cont</Text>
                    </TouchableOpacity>
                    <View style={styles.logoBox}>
                    <Image  source={require('./img/Logo.png')} style={styles.logo}></Image>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Georgia',
        flex: 1,
        backgroundColor: 'white',
        textAlign: 'center',
        justifyContent:'center'
        
    },
    credentialContainer:{
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'center'
    },
    credentials: {
        flex:1,
        justifyContent:'center',
        textAlign:'center'
    },
    credential: {
       marginLeft:'3%',
       marginTop:'2%',
       borderBottomColor:'gray',
       borderBottomWidth: 1,
      textAlign: 'center',
       width:'80%'
     
    },
    submit: {
        textAlign: 'center',
        fontSize: 24,
        fontWeight: 'bold',
        margin: '2%'
    },
    buttons: {
        flex:1,
    },
    button1:{
        backgroundColor:"#101010",
        flex:1,
        justifyContent:'center',
        margin:'4%',

    },
    submit:{
        color:'white',
        textAlign:'center',
        textTransform:'uppercase',
        fontWeight:'bold'
    },
    button2:{
        backgroundColor:"#b30505",
        flex:1,
        justifyContent:'center',
        margin:'4%',

    },
    logoBox: {
     flex:1,
     justifyContent:"center", 
     alignContent:'center',
     alignItems:'center' 
    },
    logo:{
        height:50,
        width:50
    }
})