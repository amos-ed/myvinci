import React from 'react';
import {View, TextInput, Alert} from 'react-native';
import { Button } from 'react-native-elements';
import EventFunctions from '../sections/EventFunctions.js';
import {UserFunctions} from '../sections/UserFunctions.js';
import firebase from 'react-native-firebase';

export class CheckIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            password: '',
            club: this.props.navigation.getParam('club'),
            user: {},
        }
    }

    getActualuser = async () => {
        const email = firebase.auth().currentUser.email;
        await UserFunctions.SearchByEmail(email)
            .then(response => {
                this.setState({ user: {
                    email: email,
                    name: response,
                }})
            })
            .catch(error => console.log(error));
        this.setState({ loading: false });
    }

    componentDidMount() {
        this.getActualuser();
    }

    checkIn = async () => {
        const id = this.props.navigation.getParam('id');
        await EventFunctions.GetEventByID(id, this.state.club)
            .then(response => {
                console.log(response);
                if (response.password === this.state.password) {
                    console.log(response.password);
                    var going = response.going;
                    if (going === undefined || goind === null) going = [];
                    going.push(this.state.user);
                    const club = EventFunctions.getClubCollectionName(this.state.club);
                    const event = firebase.firestore().collection('activities').doc('WeeklyActivities').collection(club).doc(id);
                    var finalObj = {
                        date: response.date,
                        description: response.description,
                        hour: response.hour,
                        location: response.location,
                        name: response.name,
                        going: going,
                        organizer: response.organizer,
                        password: response.password,
                        shortDescription: response.shortDescription
                    }
                    event.set(finalObj)
                        .then(() => {
                            Alert.alert("Ai fost inregistrat cu succes la aceasta activitate");
                            this.props.navigation.navigate('HomepageRT');
                        })
                        .catch(error => console.log(error));
                    
                }
            })
            .catch(error => console.log(error));
    }

    render() {
        if (this.state.loading === true) return <View></View>
        else return (
            <View>
                <TextInput 
                    placeholder = "Introdu codul activitatii"
                    onChangeText = {(text) => this.setState({ password: text })}
                    value = {this.state.password}
                />
                <Button title = "Verifica!" onPress = {this.checkIn} />
            </View>
        )
    }
}