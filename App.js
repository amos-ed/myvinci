import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, PermissionsAndroid } from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import { Login } from './AppScreens/activities/Login';
import { Register } from './AppScreens/activities/Register';
import { Promote } from './AppScreens/activities/CD&BD_Promote';
import { Homepage } from './AppScreens/activities/Homepage';
import { Clubs } from './AppScreens/activities/Clubs';
import { Announcements } from './AppScreens/activities/Announcements';
import { Vote } from './AppScreens/activities/Vote';
import { Events } from './AppScreens/activities/Events';
import { VotesList } from './AppScreens/activities/VotesList.js';
import { ClubEventList } from './AppScreens/activities/ClubEventList.js';
import { CreateEvent } from './AppScreens/activities/CreateEvent.js';
import { CreateAnnouncement } from './AppScreens/activities/CreateAnnouncement.js';
import { CreateVote } from './AppScreens/activities/CreateVote.js';
import { EventPage } from './AppScreens/activities/EventPage.js';
import { CheckIn } from './AppScreens/activities/CheckIn.js';
import firebase from 'react-native-firebase';

const initialRoute = firebase.auth().currentUser !== null ? "HomepageRT" : "LoginRT";

const Routes = createStackNavigator({
  LoginRT: Login,
  RegisterRT: Register,
  PromoteRT: Promote,
  HomepageRT: Homepage,
  ClubsRT: Clubs,
  AnnouncementsRT: Announcements,
  VoteRT: Vote,
  EventsRT: Events,
  VotesListRT: VotesList,
  ClubEventListRT: ClubEventList,
  CreateEventRT: CreateEvent,
  CreateAnnouncementRT: CreateAnnouncement,
  CreateVoteRT: CreateVote,
  EventPageRT: EventPage,
  CheckInRT: CheckIn,
},
  {
    initialRouteName: initialRoute
  }
)

const AppRoutes = createAppContainer(Routes);

export default class App extends Component {

  async componentDidMount() {
    try {
      firebase.messaging().subscribeToTopic("events");
      firebase.notifications().removeAllDeliveredNotifications();
      const channel = new firebase.notifications.Android.Channel('AnotherID', 'Another Channel', firebase.notifications.Android.Importance.Max).setDescription('My apps test channel');
      firebase.notifications().android.createChannel(channel); 
      const res = await firebase.messaging().requestPermission();
      const fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        console.log('FCM Token: ', fcmToken);
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
          console.log('FCM messaging has permission:' + enabled)
        } else {
          try {
            await firebase.messaging().requestPermission();
            console.log('FCM permission granted')
          } catch (error) {
            console.log('FCM Permission Error', error);
          }
        }
        firebase.notifications().onNotificationOpened((notification) => {
          // Process your notification as required
          // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
          console.log('Notification opened: ', notification)
        });

        firebase.notifications().getInitialNotification((notification) => {
          console.log("Initial notification: ", notification);
        })

        this.notificationListener = firebase.notifications().onNotification((notification) => {
          console.log('Notification: ', notification)
          const localNotification = new firebase.notifications.Notification({
            sound: 'default',
            show_in_foreground: true,
          })
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification.body)
            .setData(notification.data)
            .android.setChannelId('AnotherID') // e.g. the id you chose above
            .android.setColor('#1B7BCE') // you can set a color here
            .android.setPriority(firebase.notifications.Android.Priority.High);
          // console.log(localNotification);
          firebase.notifications().displayNotification(localNotification)
            .catch(error => console.log(error));
        });
      }
      else {
        console.log("FCM token not available");
      }
    }
    catch (e) {
      console.log(e);
    }
    
  }

  render() {
    console.disableYellowBox = true;
    return <AppRoutes />
  }
}
